// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;

import frc.robot.subsystems.DrivetrainSubsystem;

/** Sets subsystems in intended states for teleoperated control. */
public class TeleopInitCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  // Whether the initialization has occurred.
  private boolean m_hasInitialized = false;

  /** Instantiates the drivetrain subsystem when creating the command. */
  public TeleopInitCommand(DrivetrainSubsystem drivetrainSubsystem) {
    m_drivetrainSubsystem = drivetrainSubsystem;

    addRequirements(drivetrainSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Set the initialization to false to ensure the command is not returned as finished upon being
    // called for the first time.
    m_hasInitialized = false;
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the neutral mode of the drive motors to brake.
    m_drivetrainSubsystem.setNeutralMode(NeutralMode.Brake);

    // Set the boolean to true so that the command can terminate.
    m_hasInitialized = true;
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    return m_hasInitialized;
  }
}
