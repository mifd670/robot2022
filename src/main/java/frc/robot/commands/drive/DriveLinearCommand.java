// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.subsystems.DrivetrainSubsystem;

/**
 * Drives the robot linearly based on power sent to the drivetrain motors for a given time.
 *
 * <p>Does not implement advanced PID control or odometry.
 */
public class DriveLinearCommand extends CommandBase {

  // Subsystems
  private final DrivetrainSubsystem m_drivetrainSubsystem;

  // Timer
  private Timer m_timer;

  // Proportion to set the drive motors.
  private double m_power;

  // Time to drive the robot.
  private double m_seconds;

  /**
   * Constructs a command that sets the linear speed of the drivetrain to the given power for a
   * certain time in seconds.
   *
   * @param drivetrainSubsystem The drivetrain subsystem.
   * @param power Proportion of the max motor output to set the drive motors on the linear axis.
   * @param seconds Time, in seconds, to drive the robot.
   */
  public DriveLinearCommand(DrivetrainSubsystem drivetrainSubsystem, double power, double seconds) {
    m_drivetrainSubsystem = drivetrainSubsystem;
    m_power = power;
    m_seconds = seconds;

    addRequirements(m_drivetrainSubsystem);
  }

  /** Initializes the command. */
  @Override
  public void initialize() {
    // Initialize the timer.
    m_timer = new Timer();
    m_timer.reset();
    m_timer.start();
  }

  /** Runs periodically while the command is scheduled. */
  @Override
  public void execute() {
    // Set the drive motors to the given proportion on the linear axis.
    m_drivetrainSubsystem.arcadeDrive(m_power, 0.0);
  }

  /** Runs when the command ends. */
  @Override
  public void end(boolean interrupted) {
    // Set the drive motors so that the robot is stationary.
    m_drivetrainSubsystem.arcadeDrive(0.0, 0.0);
  }

  /**
   * @return Whether the command has finished. Once a command finishes, the scheduler will call its
   *     end() method and un-schedule it.
   */
  @Override
  public boolean isFinished() {
    // End the command if the total time to execute has elapsed.
    return m_timer.hasElapsed(m_seconds);
  }
}
