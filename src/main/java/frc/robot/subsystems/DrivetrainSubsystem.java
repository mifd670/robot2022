// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import frc.robot.Constants.DrivetrainConstants;
import frc.robot.Constants.RoboRIO.CAN;

/**
 * Represents the drivetrain subsystem.
 *
 * <p>Encompasses the wheels of the robot, as well as the motors that provide them power.
 */
public class DrivetrainSubsystem extends SubsystemBase {

  /** Represents values to which the drive motors will be set. */
  public enum DriveSpeed {
    kSlow(DrivetrainConstants.kSpeedSlow),
    kNormal(DrivetrainConstants.kSpeedNormal),
    kFast(DrivetrainConstants.kSpeedFast),
    kMax(1.0);

    public final double value;

    DriveSpeed(double value) {
      this.value = value;
    }
  }

  // Motor Controllers
  private final WPI_TalonSRX m_motorFrontLeft = new WPI_TalonSRX(CAN.kPortMotorDriveFrontLeft);
  private final WPI_TalonSRX m_motorFrontRight = new WPI_TalonSRX(CAN.kPortMotorDriveFrontRight);
  private final WPI_TalonSRX m_motorBackLeft = new WPI_TalonSRX(CAN.kPortMotorDriveBackLeft);
  private final WPI_TalonSRX m_motorBackRight = new WPI_TalonSRX(CAN.kPortMotorDriveBackRight);

  // Differential Drive
  private final DifferentialDrive m_differentialDrive =
      new DifferentialDrive(m_motorFrontLeft, m_motorFrontRight);

  /** Initializes the drivetrain subsystem. */
  public DrivetrainSubsystem() {
    // Revert all motor controller configurations to their factory default values.
    m_motorFrontLeft.configFactoryDefault();
    m_motorFrontRight.configFactoryDefault();
    m_motorBackLeft.configFactoryDefault();
    m_motorBackRight.configFactoryDefault();

    // Invert the right side motors to account for their default negation.
    m_motorFrontRight.setInverted(true);
    m_motorBackRight.setInverted(true);

    // Set the back motors to follow their front side counterparts.
    m_motorBackLeft.follow(m_motorFrontLeft);
    m_motorBackRight.follow(m_motorFrontRight);
  }

  /**
   * Sets the neutral mode of the drive motors.
   *
   * @param mode The neutral mode.
   */
  public void setNeutralMode(NeutralMode mode) {
    m_motorFrontLeft.setNeutralMode(mode);
    m_motorFrontRight.setNeutralMode(mode);
    m_motorBackLeft.setNeutralMode(mode);
    m_motorBackRight.setNeutralMode(mode);
  }

  /**
   * Sets the maximum speed the robot will drive.
   *
   * @param speed {@link DriveSpeed} constant to which the max motor output will be set.
   */
  public void setDriveSpeed(DriveSpeed speed) {
    m_differentialDrive.setMaxOutput(speed.value);
  }

  /**
   * Drives the robot using proportions of max motor output.
   *
   * @param xSpeed Speed along the X axis [-1.0..1.0], where forward is positive.
   * @param zRotation Rotation rate around the Z axis [-1.0..1.0], where clockwise is positive.
   */
  public void arcadeDrive(double xSpeed, double zRotation) {
    m_differentialDrive.arcadeDrive(xSpeed, zRotation);
  }

  /**
   * Drives the robot using voltages.
   *
   * @param leftVoltage Left motor output.
   * @param rightVoltage Right motor output.
   */
  public void tankDrive(double leftVoltage, double rightVoltage) {
    m_motorFrontLeft.setVoltage(leftVoltage);
    m_motorFrontRight.setVoltage(-rightVoltage);
    m_differentialDrive.feed();
  }
}
