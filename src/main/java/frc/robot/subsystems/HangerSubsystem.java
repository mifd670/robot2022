// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkMaxRelativeEncoder.Type;

import frc.robot.Constants.HangerConstants;
import frc.robot.Constants.RoboRIO.CAN;
import frc.robot.Constants.RoboRIO.DIO;

/**
 * Represents the hanger subsystem.
 *
 * <p>Encompasses the motors, servos, and limit switches that regulate the hanger.
 */
public class HangerSubsystem extends SubsystemBase {

  // Motor Controllers
  private final CANSparkMax m_motorLeft =
      new CANSparkMax(CAN.kPortMotorHangerLeft, MotorType.kBrushed);
  private final CANSparkMax m_motorRight =
      new CANSparkMax(CAN.kPortMotorHangerRight, MotorType.kBrushed);

  // Quadrature Encoders
  private final RelativeEncoder m_encoderLeft;
  private final RelativeEncoder m_encoderRight;

  // Limit Switches
  private final DigitalInput m_switchLeft = new DigitalInput(DIO.kPortLimitSwitchLeftHanger);
  private final DigitalInput m_switchRight = new DigitalInput(DIO.kPortLimitSwitchRightHanger);

  // Servos
  private final Servo m_servoLeft = new Servo(DIO.kPortServoLeftHanger);
  private final Servo m_servoRight = new Servo(DIO.kPortServoRightHanger);

  /** Initializes the hanger subsystem. */
  public HangerSubsystem() {
    // Set the motors in brake mode.
    m_motorLeft.setIdleMode(IdleMode.kBrake);
    m_motorRight.setIdleMode(IdleMode.kBrake);

    // Set the encoder to motor.
    m_encoderLeft = m_motorLeft.getEncoder(Type.kQuadrature, HangerConstants.kEncoderCyclesPerRev);
    m_encoderRight =
        m_motorRight.getEncoder(Type.kQuadrature, HangerConstants.kEncoderCyclesPerRev);

    // Reset the encoder readings.
    m_encoderLeft.setPosition(0);
    m_encoderRight.setPosition(0);
  }

  /** This method is run periodically. */
  @Override
  public void periodic() {
    SmartDashboard.putNumber("Hanger Encoder Left", getEncoderLeft());
    SmartDashboard.putNumber("Hanger Encoder Right", getEncoderRight());
  }

  /**
   * Sets the hanger motors to states such that the hanger is raised.
   *
   * @param power Proportion of the max motor output to set the hanger motors.
   */
  public void extendHanger(double power) {
    m_motorLeft.set(-power);
    m_motorRight.set(power);
  }

  /**
   * Sets the hanger motors in states such that the hanger is lowered.
   *
   * @param power Proportion of the max motor output to set the hanger motors.
   */
  public void retractHanger(double power) {
    if (isLimitSwitchLeftPressed()) m_motorLeft.set(0);
    else m_motorLeft.set(power);

    if (isLimitSwitchRightPressed()) m_motorRight.set(0);
    else m_motorRight.set(-power);
  }

  /** Disables the hanger motors. */
  public void disableHanger() {
    // Apply no voltage to the motors.
    m_motorLeft.set(0);
    m_motorRight.set(0);
  }

  /** @return Whether or not the left hanger limit switch is pressed. */
  public boolean isLimitSwitchLeftPressed() {
    return !m_switchLeft.get();
  }

  /** @return Whether or not the right hanger limit switch is pressed. */
  public boolean isLimitSwitchRightPressed() {
    return !m_switchRight.get();
  }

  /** @return The value of the left hanger encoder. */
  public double getEncoderLeft() {
    return Math.PI * (HangerConstants.kHexShaftDiam) * m_encoderLeft.getPosition();
  }

  /** @return The value of the right hanger encoder. */
  public double getEncoderRight() {
    return Math.PI * (HangerConstants.kHexShaftDiam) * m_encoderRight.getPosition();
  }

  /**
   * Sets the left hanger servo to a position that corresponds to its range.
   *
   * @param position Position [0.0..1.0] to set the left hanger servo.
   */
  public void setLeftServo(double position) {
    m_servoLeft.set(position);
  }

  /**
   * Sets the right hanger servo to a position that corresponds to its range.
   *
   * @param position Position [0.0..1.0] to set the right hanger servo.
   */
  public void setRightServo(double position) {
    m_servoRight.set(position);
  }
}
