// Copyright (c) OpenRobotGroup.
// Open Source Software; you can modify and/or share it under the terms of
// the BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import frc.robot.Constants.IntakeConstants;
import frc.robot.Constants.RoboRIO.CAN;
import frc.robot.Constants.RoboRIO.DIO;

/**
 * Represents the intake subsystem.
 *
 * <p>Encompasses the motor and double solenoid used to collect cargo.
 */
public class IntakeSubsystem extends SubsystemBase {

  /** Represents values to which the intake raising and lowering motor will be set. */
  public enum IntakeSpeed {
    kOff(0.0),
    kLower(IntakeConstants.kSpeedLower),
    kRaise(IntakeConstants.kSpeedRaise);

    public final double value;

    IntakeSpeed(double value) {
      this.value = value;
    }
  }

  // Motor Controllers
  private final CANSparkMax m_motorIntake =
      new CANSparkMax(CAN.kPortMotorIntake, MotorType.kBrushed);

  // Solenoids
  private final DoubleSolenoid m_doubleSolenoidIntake =
      new DoubleSolenoid(
          PneumaticsModuleType.CTREPCM,
          CAN.kPortDoubleSolenoidIntake[0],
          CAN.kPortDoubleSolenoidIntake[1]);

  // Digital Inputs
  private final DigitalInput m_limitSwitchLower = new DigitalInput(DIO.kPortLimitSwitchLowerIntake);
  private final DigitalInput m_limitSwitchUpper = new DigitalInput(DIO.kPortLimitSwitchUpperIntake);

  /**
   * Sets the speed the intake motor raises or lowers depending on limit switch values.
   *
   * @param speed {@link IntakeSpeed} constant to which the intake motor will be set.
   */
  public void setIntakeSpeed(IntakeSpeed speed) {
    // Lowers the intake motor until a lower limit switch is pressed.
    if (speed.value < 0 && !isLimitSwitchLowerPressed()) m_motorIntake.set(speed.value);
    // Raises the intake motor until an upper limit switch is pressed.
    else if (speed.value > 0 && !isLimitSwitchUpperPressed()) m_motorIntake.set(speed.value);
    // Sets the intake motor speed to 0 when no input is given or a limit switch is pressed.
    else m_motorIntake.set(IntakeSpeed.kOff.value);
  }

  /** @return The state of the intake's grabber solenoid. */
  public DoubleSolenoid.Value getGrabber() {
    return m_doubleSolenoidIntake.get();
  }

  /**
   * Sets the grabber solenoid in the specified state.
   *
   * @param value Value to set the grabber solenoid.
   */
  public void setGrabber(DoubleSolenoid.Value value) {
    m_doubleSolenoidIntake.set(value);
  }

  /** Toggles the double solenoid to extend or retract the intake's grabber. */
  public void toggleGrabber() {
    if (getGrabber() == Value.kOff) setGrabber(DoubleSolenoid.Value.kReverse);
    m_doubleSolenoidIntake.toggle();
  }

  /** @return Whether or not the lower intake limit switch is pressed. */
  public boolean isLimitSwitchLowerPressed() {
    return !m_limitSwitchLower.get();
  }

  /** @return Whether or not the upper intake limit switch is pressed. */
  public boolean isLimitSwitchUpperPressed() {
    return !m_limitSwitchUpper.get();
  }
}
